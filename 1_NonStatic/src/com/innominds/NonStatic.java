package com.innominds;

public class NonStatic {
 
	private  int count = 0;
	
	public NonStatic() {
		count++;
	}
	
	public static void main(String[] args) {

		NonStatic n1 =new NonStatic(); 
		NonStatic n2 =new NonStatic(); 
		NonStatic n3 =new NonStatic(); 
		NonStatic n4 =new NonStatic(); 
		
		System.out.println(n2.count);
	}

}
