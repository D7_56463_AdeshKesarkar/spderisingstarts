package com.innominds.assignment.question23;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.jupiter.api.Test;

public class Question23Test {

	
	@Test
	void Question23Test() {
		Question23 q1 = new Question23(1, "saurabh", "spde");
		Question23 q2= new Question23(2, "syed", "spde");
		Question23 q3= new Question23(3, "sammy", "spde");
		
		ArrayList<Question23> emp = new  ArrayList<>();
		ArrayList<Question23> emp2 = new  ArrayList<>();
		emp.add(q1);
		emp.add(q2);
		emp.add(q3);
		
		emp2.add(q1);
		emp2.add(q2);
		emp2.add(q3);
		
		
		
		Collections.sort(emp, (a,b)-> {return (a.getName()).compareTo(b.getName()); });
		Collections.sort(emp2);
		System.out.println(emp);
		System.out.println(emp2);
		assertTrue(emp2.equals(emp));
				
	}
}
