package com.innominds.assignment.question11;

public class Vehicle {

	private int direction;
	private int route;

	public Vehicle(int direction, int route) {
		this.direction = direction;
		this.route = route;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getRoute() {
		return route;
	}

	public void setRoute(int route) {
		this.route = route;
	}
}

