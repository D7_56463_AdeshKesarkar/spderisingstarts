package com.innominds.assignment.question11;

/*
 * On a single track, two vehicles are running. As vehicles are going in same
 * direction there is no problem. If the vehicles are running
 * in opposition direction there is a chance of collision. To
 * avoid collisions write a java program using exception handling. You are free
 * to make necessary assumptions. 
 */

public class Question11 {

	public static String collision(Vehicle v1, Vehicle v2) throws CustomException {
		if (v1.getRoute() != v2.getRoute()) {
			return "No Collision";
		} else {
			if (v1.getDirection() == v2.getDirection()) {
				return "No Collision Occur . Both Vehicles are in same direction";
			}else {
				 throw new CustomException("Collision Occurs!!!! Please slow down");
			}
		}
	}
	
	public static void main(String[] args) throws CustomException {
		Vehicle v1 = new Vehicle(0, 0);
		Vehicle v2 = new Vehicle(1, 0);
		
		System.out.println(collision(v1, v2));
		
	}
}
