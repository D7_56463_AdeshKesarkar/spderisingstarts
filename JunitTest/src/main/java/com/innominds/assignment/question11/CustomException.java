package com.innominds.assignment.question11;

public class CustomException extends Exception {

	private String message;

	public CustomException(String message) {
		super(message);
	}

}
