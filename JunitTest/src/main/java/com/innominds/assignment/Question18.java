package com.innominds.assignment;

import java.util.Scanner;

import org.springframework.context.annotation.ScannedGenericBeanDefinition;

public class Question18 {

//	Define a “Clock” class that does the following: - 
//	a. Accept hours, minutes and seconds. 
//	b. Check the validity numbers. 
//	c. Set the time to AM/PM mode. 
//	Use necessary constructors and methods to do the above task. 

	private int hour;
	private int minutes;
	private int seconds;
	private String phase;

	public Question18() {

	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		if (hour <= 24)
			if (hour <= 12)
				this.hour = hour;
			else
				this.hour = hour % 12;
		else {
			this.hour = hour % 24;
		}
	}

	public int getMinutes() {
		return minutes;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(int hr) {
		if (hr < 24) {
			if (hr < 12)
				this.phase = "AM";
			else
				this.phase = "PM";

		}

	}

	public void setMinutes(int minutes) {
		if (minutes <= 60)
			this.minutes = minutes;
		else
			this.minutes = minutes % 60;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		if (seconds <= 60)
			this.seconds = seconds;
		else
			this.seconds = seconds % 60;
	}

	@Override
	public String toString() {
		return "hours=" + hour + ", minutes=" + minutes + ", seconds=" + seconds + ", phase=" + phase;
	}

}
