package com.innominds.assignment.question20;

import java.util.ArrayList;
import java.util.List;

public class RoundRobin {

	private List<Question20> jobList ;
	private List<Question20> readyQueue;
	private List<Question20> executionQueue;
	
	public RoundRobin() {
		jobList = new ArrayList<>();
		readyQueue = new ArrayList<>();
		executionQueue = new ArrayList<>();
		
	}

	public List<Question20> getJobList() {
		return jobList;
	}

	public void setJobList(List<Question20> jobList) {
		this.jobList = jobList;
	}

	public List<Question20> getReadyQueue() {
		return readyQueue;
	}

	public void setReadyQueue(List<Question20> readyQueue) {
		this.readyQueue = readyQueue;
	}

	public List<Question20> getExecutionQueue() {
		return executionQueue;
	}

	public void setExecutionQueue(List<Question20> executionQueue) {
		this.executionQueue = executionQueue;
	}

	
	public void completionTime() {
		for (int i = 0; i < jobList.size(); i++) {
			
		}
	}
}
