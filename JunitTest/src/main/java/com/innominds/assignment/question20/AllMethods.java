package com.innominds.assignment.question20;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class AllMethods {

	private ArrayList<Question20> jobs;

	public AllMethods() {
		jobs = new ArrayList<>();
	}

	public ArrayList<Question20> getJobs() {
		return jobs;
	}

	public void setJobs(ArrayList<Question20> jobs) {
		this.jobs = jobs;
	}

	public void calculateWaitingTime() {
		if (jobs.isEmpty())
			System.out.println("Not any job in queue...!!!");
		else {

			jobs.get(0).setWaitingTime(0);
			for (int i = 1; i < jobs.size(); i++) {
				jobs.get(i).setWaitingTime(jobs.get(i).getTurnAroundTime() - jobs.get(i).getBurstTime());
			}
		}
	}

	public void calculateTAT() {
		if (jobs.isEmpty())
			System.out.println("Not any job in queue...!!!");
		else {
			for (Question20 q : jobs) {
				q.setTurnAroundTime(q.getCompletionTime() - q.getArrivalTime());
			}
		}
	}

	public double calculateAvgWaitingTime() {
		int waitingTime = 0;
		for (Question20 q : jobs) {
			waitingTime += q.getWaitingTime();
		}
		return waitingTime / (jobs.size());
	}

	public double calculateAvgTATTime() {
		int TaTTime = 0;
		for (Question20 q : jobs) {
			TaTTime += q.getTurnAroundTime();
		}
		return TaTTime / (jobs.size());
	}

//	public void fcfs() {
//		Collections.sort(jobs, new Comparator<Question20>() {
//
//			public int compare(Question20 o1, Question20 o2) {
//				return ((Integer) o1.getArrivalTime()).compareTo(o2.getArrivalTime());
//			}
//		});
//	}

	public void completionTime() {
		if (jobs.isEmpty())
			System.out.println("Not any job in queue...!!!");
		else {
			for (int i = 0; i < jobs.size(); i++) {
				if (i == 0) {
					jobs.get(i).setCompletionTime(jobs.get(i).getBurstTime() + jobs.get(i).getArrivalTime());
				} else {
					if (jobs.get(i).getArrivalTime() > jobs.get(i - 1).getCompletionTime())
						jobs.get(i).setCompletionTime((jobs.get(i).getArrivalTime()) + (jobs.get(i).getBurstTime()));
					else
						jobs.get(i).setCompletionTime(
								(jobs.get(i - 1).getCompletionTime()) + (jobs.get(i).getBurstTime()));
				}
			}
		}

	}

	public void fcfs() {
		Collections.sort(jobs, (o1, o2) -> {
			return ((Integer) o1.getArrivalTime()).compareTo(o2.getArrivalTime());
		});
	}

	public void sjf() {
		Collections.sort(jobs,
				Comparator.comparing((Question20 q) -> q.getArrivalTime()).thenComparing(q -> q.getBurstTime()));
	}

	public void priority() {
		Collections.sort(jobs, (o1, o2) -> {
			return ((Integer) o1.getPriority()).compareTo(o2.getPriority());
		});
	}

	
	public static void main(String[] args) {

		Question20 q1 = new Question20("q1", 1, 3, 1);
		Question20 q2 = new Question20("q2", 2, 4, 1);
		Question20 q3 = new Question20("q3", 1, 2, 1);
		Question20 q4 = new Question20("q4", 4, 4, 1);
//		Question20 q5 = new Question20("q5", 5, 5, 1);
//		Question20 q6 = new Question20("q6", 6, 5, 1);

		AllMethods a = new AllMethods();
//		a.jobs.add(q6);
//		a.jobs.add(q5);
		a.jobs.add(q4);
		a.jobs.add(q3);
		a.jobs.add(q2);
		a.jobs.add(q1);

		System.out.println(a.getJobs());
		a.fcfs();
		a.completionTime();
		a.calculateTAT();
		a.calculateWaitingTime();
		System.out.println(a.getJobs());
		System.out.println(" Avg waiting time = " + a.calculateAvgWaitingTime());
		System.out.println(" Avg TAT time = " + a.calculateAvgTATTime());

//		a.sjf();
//		a.completionTime();
//		a.calculateTAT();
//		a.calculateWaitingTime();
//		System.out.println(a.getJobs());

		/* Round Robin */

	}

}
