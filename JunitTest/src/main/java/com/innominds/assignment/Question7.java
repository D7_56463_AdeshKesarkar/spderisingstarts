package com.innominds.assignment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Question7 {
	/*
	 * 7.Write unit tests for a program to prepare a method to accept 2 dates as
	 * parameter and return the difference between those 2 dates. 
	 */

	private static SimpleDateFormat sdf;

	static {
		sdf = new SimpleDateFormat("dd/MM/yyyy");
	}

	public static  long calculateDifferenceBetweenDates(String d1, String d2) throws ParseException {
		Date dd1 = sdf.parse(d1);
		Date dd2 = sdf.parse(d2);

		return dd1.getDate() - dd2.getDate();
	}

	public static void main(String[] args) throws ParseException {
		
	}
}
