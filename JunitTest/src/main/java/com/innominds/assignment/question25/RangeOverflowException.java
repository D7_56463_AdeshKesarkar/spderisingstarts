package com.innominds.assignment.question25;

@SuppressWarnings("serial")
public class RangeOverflowException extends Exception {

	public RangeOverflowException(String message) {
		super(message);
	}

}
