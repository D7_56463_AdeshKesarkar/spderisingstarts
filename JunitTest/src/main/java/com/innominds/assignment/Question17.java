package com.innominds.assignment;

import java.util.Arrays;

public class Question17 {

	public static void main(String[] args) {
		System.out.println("Prime Number : ");
		
		int[] arr = new int[args.length];
		
		for (int i = 0; i < arr.length; i++) {
			arr[i] = Integer.parseInt(args[i]);
		}
		
		System.out.println(Arrays.toString(arr));
		
		
		for (int i = 0; i < arr.length; i++) {
			boolean isPrime = true;
			
			if(arr[i] == 0 || arr[i] == 1)
				isPrime = false;
			for (int j = 2; j <= arr[i]/2; j++) {

	            if(arr[i]%j==0){
	                isPrime = false;
	                break;
	            }
			}
			
			if(isPrime) {
				System.out.print(arr[i] + " ");
			}
		}
	}
}
