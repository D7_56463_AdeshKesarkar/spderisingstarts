package com.innominds.assignment;

public class Question1 {

	private int count = 0;
	private static int staticCount = 0;

	public Question1() {
		staticCount++;
		count++;
	}

	public static int getStaticCount() {
		return staticCount;
	}

	public static void setStaticCount(int staticCount) {
		Question1.staticCount = staticCount;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
