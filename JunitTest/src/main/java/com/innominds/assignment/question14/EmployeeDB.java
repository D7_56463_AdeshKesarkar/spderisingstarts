package com.innominds.assignment.question14;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

public class EmployeeDB {
	private ArrayList<Employee> empAList = new ArrayList<>();
	private TreeSet<Employee> empTList = new TreeSet<>();

	public ArrayList<Employee> getEmpAList() {
		return empAList;
	}

	public void setEmpAList(ArrayList<Employee> empAList) {
		this.empAList = empAList;
	}

	public TreeSet<Employee> getEmpTList() {
		return empTList;
	}

	public void setEmpTList(TreeSet<Employee> empTList) {
		this.empTList = empTList;
	}

	public boolean addEmployee(Employee e) {

		return empAList.add(e);
	}

	@Override
	public String toString() {
		return "EmployeeDB [empAList=" + empAList + ", empTList=" + empTList + "]";
	}

	public boolean deleteEmployee(int empId) throws Exception {
		Employee e = new Employee(empId);
		int index = empAList.indexOf(e);
		if (index == -1)
			throw new RuntimeException("User not present with this empId");
		else {

			Employee e1 = empAList.get(index);
			return empAList.remove(e1);
		}

	}

	public String showPaySlip(int empId) {
		Employee e = new Employee(empId);
		int index = empAList.indexOf(e);
		if (index == -1)
			throw new RuntimeException("User not present with this empId");
		else {
			Employee e1 = empAList.get(index);
			return String.valueOf(e1.getEmpSalary());
		}

	}

	public Employee[] listAll() {
		if (empAList.isEmpty())
			return new Employee[0];
		else {
			return empAList.toArray(new Employee[empAList.size()]);
		}
	}

	public boolean addEmplotyeeTS(Employee e) {
		return empTList.add(e);
	}

	public boolean deleteEmpTS(int empId) {
		Employee e = new Employee(empId);
		Iterator<Employee> iterator = empTList.iterator();
		while (iterator.hasNext()) {
			if (e.equals(iterator.next())) {
				return empTList.remove(e);
			}
		}
		return false;
	}

	public String payslipTS(int empId) {
		Employee e = new Employee(empId);
		Iterator<Employee> iterator = empTList.iterator();
		while (iterator.hasNext()) {
			Employee eee = iterator.next();
			if (e.equals(eee)) {
				return String.valueOf(eee.getEmpSalary());
			}
		}
		throw new RuntimeException("Emp not found...!!!");

	}

	public Employee[] listAllTS() {
		return empTList.toArray(new Employee[empTList.size()]);
	}
}
