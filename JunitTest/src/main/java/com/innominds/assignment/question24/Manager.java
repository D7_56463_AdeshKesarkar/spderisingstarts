package com.innominds.assignment.question24;

public class Manager extends Employee {

	private String department;

	public Manager(int empId, String name, double salary, double bonus, String email, String department) {
		super(empId, name, salary, bonus, email);
		this.department = department;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Override
	public double netSalary(Employee e) {
		return super.netSalary(e);
	}

	public static void main(String[] args) {
		Manager m = new Manager(1, "manager", 12000, 500, "m@gmail.com", "SPDE");
		System.out.println(m.netSalary(m));
	}

	@Override
	public String toString() {
		return super.toString() + " department = " + department;
	}

}
