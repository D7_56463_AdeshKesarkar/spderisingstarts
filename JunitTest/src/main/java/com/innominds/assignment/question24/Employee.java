package com.innominds.assignment.question24;

public abstract class Employee {

	private int empId;
	private String name;
	private double salary;
	private double bonus;
	private String email;
	public Employee(int empId, String name, double salary, double bonus, String email) {
		this.empId = empId;
		this.name = name;
		this.salary = salary;
		this.bonus = bonus;
		this.email = email;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public double getBonus() {
		return bonus;
	}
	public void setBonus(double bonus) {
		this.bonus = bonus;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Employee empId=" + empId + ", name=" + name + ", salary=" + salary + ", bonus=" + bonus + ", email="
				+ email ;
	}
	
	public double netSalary(Employee e) {
		return e.getSalary() + e.getBonus();
	}
	
}
