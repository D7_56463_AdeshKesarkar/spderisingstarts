package com.innominds.assignment;

import java.util.function.BiConsumer;

interface addition {
	int add(int a, int b);
}
//6.Write unit tests for prepare addition of two numbers method and implement the same using lambda expression. 
public class Question22 {

	public int sumOfNumber(int a, int b) {
		return a + b;
	}

	public static void main(String[] args) {
		addition sum = (int a, int b) -> a+b;
		System.out.println(sum.add(11, 22));
	}
}
