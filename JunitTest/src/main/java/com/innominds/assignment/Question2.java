package com.innominds.assignment;

public class Question2 {

	/*
	 * 2.Write unit tests for a program to check the input characters for uppercase,
	 * lowercase, number of digits and other characters 
	 */
	private static StringBuilder upper = new StringBuilder();
	private static StringBuilder lower = new StringBuilder();
	private static StringBuilder digits = new StringBuilder();
	private static StringBuilder other = new StringBuilder();

	public static String findDigits(String s) {
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (ch >= 48 && ch <= 57) {
				digits.append(ch);
			}
		}
		return digits.toString();
	}

	public static String findLowerCase(String s) {
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (ch >= 97 && ch <= 122) {
				lower.append(ch);
			}
		}
		return lower.toString();
	}

	public static String findUpperCase(String s) {
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (ch >= 65 && ch <= 90) {
				upper.append(ch);
			}
		}
		return upper.toString();
	}

	public static String findOthers(String s) {
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (!(ch >= 65 && ch <= 90) && !(ch >= 97 && ch <= 122) && !(ch >= 48 && ch <= 57)) {
				other.append(ch);
			}
		}
		return other.toString();
	}

	public static void main(String[] args) {
		Question2 q2 = new Question2();

		String s1 = "ASDF";

		String s2 = findUpperCase(s1);
		System.out.println(s2);
	}
}
