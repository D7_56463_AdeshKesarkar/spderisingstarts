package com.innominds.assignment.question23;

import java.util.ArrayList;

public class Employee {
	private ArrayList<Question23> emp;

	public Employee() {
		emp = new ArrayList<>();
	}

	public ArrayList<Question23> getEmp() {
		return emp;
	}

	public void setEmp(ArrayList<Question23> emp) {
		this.emp = emp;
	}

	@Override
	public String toString() {
		return "Employee [emp=" + emp + "]";
	}



}
