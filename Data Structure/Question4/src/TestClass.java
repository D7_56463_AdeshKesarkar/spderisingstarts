import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class TestClass {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {

			List<Emplooyee> allEmployees = new ArrayList<Emplooyee>();
			allEmployees.add(new Emplooyee(1, "Adesh", "Kesakar", "ak@test.com"));
			allEmployees.add(new Emplooyee(8, "Bhushan", "Raut", "br@test.com"));
			allEmployees.add(new Emplooyee(2, "Kunal", "Asnani", "ka@test.com"));
			allEmployees.add(new Emplooyee(3, "Shubham", "Hore", "sh@test.com"));
			allEmployees.add(new Emplooyee(4, "Kunal", "Jai", "kj@test.com"));
			allEmployees.add(new Emplooyee(5, "Vaibhav", "Sahu", "vai@test.com"));
			allEmployees.add(new Emplooyee(7, "Amit", "Anwade", "aa@test.com"));

			boolean exit = false;
			while (!exit) {

				System.out.println("*****Enter the choice******");
				System.out.println("1. Choose a random employee");
				System.out.println("2. Get List of Unique First Names");
				System.out.println("3. Count the number of Employees with Each Name");
				System.out.println("4. To add Employee to the Waiting Ticket List");
				System.out.println("5. To remove Employee from the Waiting Ticket List");
				System.out.println("6. Exit");

				int choice = sc.nextInt();
				switch (choice) {
				case 1:
					Emplooyee  emp = EmpUtils.getRandomEmp(allEmployees);
					System.out.println(emp);
					break;

				case 2:
					List<String> uniqueNames = EmpUtils.setEmpName(allEmployees);
					System.out.println(uniqueNames);
					break;

				case 3:
					Map<String, Integer> mostPopularNames = EmpUtils.mostPopularEmp(allEmployees);
					for (Map.Entry<String, Integer> entry : mostPopularNames.entrySet()) {
						System.out.println(
								"Name : " + entry.getKey() + " No. of Employees with This name : " + entry.getValue());
					}
					break;

				case 4:
					System.out.println("Enter empId, firstName, LastName, Email");
					Emplooyee newEmployee = new Emplooyee (sc.nextInt(), sc.next(), sc.next(), sc.next());
					System.out.println(EmpUtils.addEmp(allEmployees, newEmployee));
					allEmployees.add(newEmployee);
					break;

				case 5:
					System.out.println("Enter empId of the employee to be removed : ");

					Emplooyee  emp1 = EmpUtils.findByEmpId(allEmployees, sc.nextInt());
					System.out.println(EmpUtils.removeEmp(allEmployees, emp1));
					allEmployees.remove(emp1);
					break;

				case 6:
					exit = true;
					break;
				}
			}
		}


	}

}
