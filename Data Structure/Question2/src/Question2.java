

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Question2 {

	private LinkedList<Integer> A1;
	private LinkedList<Integer> A2;

	public Question2() {
		A1 = new LinkedList<>();
		A2 = new LinkedList<>();
	}

	public List<Integer> saveEvenNumber(int N) {
		for (int i = 2; i <= N; i++) {
			if (i % 2 == 0) {
				A1.add(i);
			}
		}
		return A1;
	}

	public List<Integer> printEvenNumber() {
		for (int i = 0; i < A1.size(); i++) {
			A2.add(A1.get(i) * 2);
			System.out.print(A1.get(i) * 2 + " ");
		}
		return A2;
	}

	public int printEvenNumber(int N) {
		return A1.contains(N) ? N : 0;
	}

	public static void main(String[] args) {
		Question2 q = new Question2();
		q.saveEvenNumber(40);
		q.printEvenNumber();
		System.out.println();
		System.out.println(q.printEvenNumber(36));
	}
}

