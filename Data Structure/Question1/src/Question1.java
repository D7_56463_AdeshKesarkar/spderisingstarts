

import java.util.ArrayList;
import java.util.List;

public class Question1 {

	private List<Integer> A1;
	private List<Integer> A2;

	public Question1() {
		A1 = new ArrayList<>();
		A2 = new ArrayList<>();
	}

	public List<Integer> saveEvenNumber(int N) {
		for (int i = 2; i <= N; i++) {
			if (i % 2 == 0) {
				A1.add(i);
			}
		}
		return A1;
	}

	public List<Integer> printEvenNumber() {
		for (int i = 0; i < A1.size(); i++) {
			A2.add(A1.get(i) * 2);
			System.out.print(A1.get(i) * 2 + " ");
		}
		return A2;
	}

	public int printEvenNumber(int N) {
		return A1.contains(N) ? N : 0;
	}

	public static void main(String[] args) {
		Question1 q = new Question1();
		q.saveEvenNumber(40);
		q.printEvenNumber();
		System.out.println();
		System.out.println(q.printEvenNumber(36));
	}
}

