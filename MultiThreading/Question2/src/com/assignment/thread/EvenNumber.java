package com.assignment.thread;

public class EvenNumber extends Thread {

	@Override
	public synchronized void run() {
		try {
			int N = 50;
			int count = 0;
			while (count < N) {
				if (count % 2 == 0) {

					sleep(1000);
					System.out.println("ping");
				}
				count++;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
