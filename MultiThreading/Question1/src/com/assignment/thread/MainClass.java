package com.assignment.thread;

public class MainClass {

	public static void main(String[] args) {
		
		GoodMorning t1 = new GoodMorning();
		Hello t2 = new Hello();
		Welcome t3 = new Welcome();
		
		t1.start();
		t2.start();
		t3.start();
		
	}
}
