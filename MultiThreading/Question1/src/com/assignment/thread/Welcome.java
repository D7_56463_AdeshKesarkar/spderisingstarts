package com.assignment.thread;

public class Welcome extends Thread {

	@Override
	public synchronized void run() {
		try {

			int i = 0;
			while(i < 5 ) {
				sleep(3000);
				System.out.println("Welcome");
				i++;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
