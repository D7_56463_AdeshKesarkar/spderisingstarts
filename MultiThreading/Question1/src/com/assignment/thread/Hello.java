package com.assignment.thread;

public class Hello extends Thread {

	@Override
	public synchronized void run() {
		int i = 0;
		while(i<5) {
			try {
				sleep(2000);
				System.out.println("Hello");
				i++;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
