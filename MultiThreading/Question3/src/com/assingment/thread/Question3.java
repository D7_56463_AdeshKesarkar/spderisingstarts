package com.assingment.thread;

public class Question3 extends Thread {

	public static int count = 0;
	public static String threadName = "";

	public Question3() {
		count++;
	}

	public void run() {
		System.out.println("Inside run method");

		for (int i = 0; i < 5; i++) {
			try {
				if (currentThread().getPriority() == 7 || currentThread().getPriority() == 6) {

					Thread.sleep(3000);
					System.out.println("Inside thread : " + currentThread().getName());
					threadName = currentThread().getName();
				} else {
					System.out.println(currentThread().getName());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws InterruptedException {

		Question3 q1 = new Question3();
		Question3 q2 = new Question3();
		Question3 q3 = new Question3();
		Question3 q4 = new Question3();
		Question3 q5 = new Question3();

		q1.setPriority(2);
		q2.setPriority(3);
		q3.setPriority(5);
		q4.setPriority(6);
		q5.setPriority(7);
		q1.start();
		q2.start();
		q3.start();
		q4.start();
		q5.start();
		Thread.sleep(40000);
		System.out.println("Last Thread : "+threadName);
	}

}
