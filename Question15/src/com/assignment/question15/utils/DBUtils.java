package com.assignment.question15.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {

	private static Connection cn; // default = null

	// add a static method to return SINGLETON( = single instance in the entire JAVA
	// App) to the caller

	public static Connection fetchConnection() throws SQLException {
		if (cn == null) {
			String url = "jdbc:mysql://localhost:3306/hackathon?useSSL=false&allowPublicKeyRetrieval=true";
			cn = DriverManager.getConnection(url, "root", "password");

		}
		return cn;
	}
}
